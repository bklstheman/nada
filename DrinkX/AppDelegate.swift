//
//  AppDelegate.swift
//  DrinkX
//
//  Created by William Kluss on 6/10/14.
//  Copyright (c) 2014 William Kluss. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
                            
    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: NSDictionary?) -> Bool {
        // Override point for customization after application launch.
        //Check NSUserdefaults to see if this is the first time loading the application
        //if so we need to load the core data tables from JSON.
        
        var userDefaults = NSUserDefaults.standardUserDefaults()
        let isDataLoaded = userDefaults.boolForKey("dataLoaded")
        
        //If the data is not loaded lets go ahead and load it
        if(!isDataLoaded) {
            userDefaults.setBool(true, forKey: "dataLoaded")
            
            let coreDataManager = CoreDataManager()
            var jsonDrinksPath = NSBundle.mainBundle().pathForResource("drinks", ofType: "json") as String
            let jsonData = NSData.dataWithContentsOfFile(jsonDrinksPath, options: NSDataReadingOptions.DataReadingMappedIfSafe, error: nil);
            var drinksJson = NSJSONSerialization.JSONObjectWithData(jsonData,
                options: NSJSONReadingOptions.MutableContainers, error: nil) as Array<Dictionary<String, AnyObject>>
            
            coreDataManager.loadDrinkIntoCoreData(drinksJson)
        }
        let coreDataManager = CoreDataManager()

        println(coreDataManager.retrieveAllDrinks().count)
        
        return true
    }
    
    // Override point for customization after application launch.
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        //self.saveContext()
    }
}


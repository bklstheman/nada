//
//  DrinkXSearchViewController.swift
//  DrinkX
//
//  Created by William Kluss on 6/14/14.
//  Copyright (c) 2014 William Kluss. All rights reserved.
//

import UIKit

class DrinkXSearchViewController: UIViewController, UISearchDisplayDelegate, UISearchBarDelegate, UITableViewDelegate {
    
    @IBOutlet var drinkSearchBar : UISearchBar = nil
    var drinkFetchedResultsController : NSFetchedResultsController?
    
    func tableView(tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        if self.drinkFetchedResultsController {
            return self.drinkFetchedResultsController!.fetchedObjects.count
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell? {
        let cell:UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        
        let drink = self.drinkFetchedResultsController!.fetchedObjects[indexPath.row] as Drink
        cell.textLabel.text = drink.name

        return cell
    }
    
    func searchDisplayController(controller: UISearchDisplayController!, shouldReloadTableForSearchString searchString: String!) -> Bool {
        
        self.filterContentForSearchText(searchString)
        
        return true
    }
    
    func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        
        let drinkDetailsVC = self.storyboard.instantiateViewControllerWithIdentifier("DrinkDetailsVC") as DrinkXDetailsViewController
        drinkDetailsVC.drink = self.drinkFetchedResultsController!.fetchedObjects[indexPath.row] as? Drink
        self.navigationController.pushViewController(drinkDetailsVC, animated: true)
    }
    
    func filterContentForSearchText(searchText : String) {
        let coreDataManager = CoreDataManager()
        self.drinkFetchedResultsController = coreDataManager.retrieveDrinkByName(searchText)
        self.searchDisplayController.searchResultsTableView.reloadData()
    }
    
}

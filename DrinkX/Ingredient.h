//
//  Ingredient.h
//  DrinkX
//
//  Created by William Kluss on 6/10/14.
//  Copyright (c) 2014 William Kluss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Ingredient : NSManagedObject

@property (nonatomic, retain) NSString * detail;

@end

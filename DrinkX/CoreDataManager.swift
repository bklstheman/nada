//
//  CoreDataManager.swift
//  DrinkX
//
//  Created by William Kluss on 6/10/14.
//  Copyright (c) 2014 William Kluss. All rights reserved.
//

import UIKit

class CoreDataManager: NSObject {
    
    class func sharedInstance() -> CoreDataManager! {
        struct Static {
            static var instance: CoreDataManager? = nil
            static var onceToken: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.onceToken) {
            Static.instance = self()
        }
        return Static.instance!
    }

    @required init() {
        
    }
        
    var _basicFetchRequest: NSFetchRequest? = nil
    
    var basicFetchRequest : NSFetchRequest {
    
    if !_basicFetchRequest {
        _basicFetchRequest = NSFetchRequest()
        _basicFetchRequest!.entity = NSEntityDescription.entityForName("Drink",
            inManagedObjectContext: self.managedObjectContext)
        }
    
    return _basicFetchRequest!
    }
    
    var _sortByNameDescriptor: NSSortDescriptor? = nil
    var sortByNameDescriptor: NSSortDescriptor {
    
    if !_sortByNameDescriptor {
        _sortByNameDescriptor = NSSortDescriptor(key: "name", ascending: true)
        }
    
    return _sortByNameDescriptor!
    }
    
    //Saves current entities open.
    func saveEntity() -> Bool {
        return self.managedObjectContext.save(nil)
    }
    
    func createFavoriteDrinkFetchedResultController() -> NSFetchedResultsController {
        let favoriteDrinkFetchRequest = self.basicFetchRequest
        favoriteDrinkFetchRequest.predicate = NSPredicate(format: "favorite = %@", argumentArray: [true])
        favoriteDrinkFetchRequest.fetchBatchSize = 40
        favoriteDrinkFetchRequest.sortDescriptors = [self.sortByNameDescriptor]
        
        let fetchedResultController = NSFetchedResultsController(fetchRequest: favoriteDrinkFetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: "FavoriteDrinkCache")

        return fetchedResultController
    }
    
    func addDrinkToFavorites(drinkName : String) -> Bool {
        let drinkByNameFetchRequest = self.basicFetchRequest
        drinkByNameFetchRequest.predicate = NSPredicate(format: "name == %@", argumentArray: [drinkName])
        drinkByNameFetchRequest.sortDescriptors = [self.sortByNameDescriptor]
        drinkByNameFetchRequest.fetchBatchSize = 1

        let drinkToAddToFavorites:Drink? = self.managedObjectContext.executeFetchRequest(drinkByNameFetchRequest, error: nil)[0] as? Drink
        
        if(drinkToAddToFavorites) {
            drinkToAddToFavorites!.favorite = true
            self.managedObjectContext.save(nil)
            return true
        } else {
            return false
        }
    }
    
    func removeDrinkFromFavotires(drinkName : String) -> Bool {
        let drinkByNameFetchRequest = self.basicFetchRequest
        drinkByNameFetchRequest.predicate = NSPredicate(format: "name == %@", argumentArray: [drinkName])
        drinkByNameFetchRequest.sortDescriptors = [self.sortByNameDescriptor]
        drinkByNameFetchRequest.fetchBatchSize = 1
        
        let drinkToAddToFavorites:Drink? = self.managedObjectContext.executeFetchRequest(drinkByNameFetchRequest, error: nil)[0] as? Drink
        
        if(drinkToAddToFavorites) {
            drinkToAddToFavorites!.favorite = false
            self.managedObjectContext.save(nil)
            return true
        } else {
            return false
        }
    }
    
    func retrieveDrinkByName(drinkName: String) -> NSFetchedResultsController {
        let drinkByNameFetchRequest = self.basicFetchRequest
        drinkByNameFetchRequest.predicate = NSPredicate(format: "name contains[c] %@", argumentArray: [drinkName])
        drinkByNameFetchRequest.sortDescriptors = [self.sortByNameDescriptor]
        drinkByNameFetchRequest.fetchBatchSize = 40
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: drinkByNameFetchRequest, managedObjectContext: self.managedObjectContext, sectionNameKeyPath: nil, cacheName: "DrinkByNameCache")
        fetchedResultsController.performFetch(nil)
        
        return fetchedResultsController;
    }
    
    func loadDrinkIntoCoreData(drinks : Array<Dictionary<String, AnyObject>>) {
        
        for drink : Dictionary<String, AnyObject> in drinks {
            //Create Drink Entity object
            var drinkEntity : Drink = NSEntityDescription.insertNewObjectForEntityForName("Drink",
                inManagedObjectContext: self.managedObjectContext) as Drink
            
            drinkEntity.name = drink["name"] as NSString
            drinkEntity.favorite = false;
            let drinkIngredients = drink["ingredients"] as NSArray
            
            //Looping and creating Ingredient Objects
            for ingredient : AnyObject in drinkIngredients{
                let ingredientEntity : Ingredient = NSEntityDescription.insertNewObjectForEntityForName("Ingredient",
                    inManagedObjectContext: self.managedObjectContext) as Ingredient
                
                ingredientEntity.detail = ingredient as String;
                drinkEntity.addIngredientsObject(ingredientEntity)
            }
            
            //Create summary for drink
            let drinkSummary = drink["instructions"] as NSArray
            var summary = String();
            for step : AnyObject in drinkSummary {
                summary = summary.stringByAppendingString(step as String + ". ")
            }
            drinkEntity.summary = summary;
            
            //Save it to core data
            self.managedObjectContext.save(nil);
        }
    }
    
    func retrieveAllDrinks() -> Array<Drink> {
        let drinkFetchRequest = self.basicFetchRequest
        let drinks = self.managedObjectContext.executeFetchRequest(drinkFetchRequest, error: nil) as Array<Drink>
        
        return drinks;
    }

// #pragma mark - Core Data stack
    func saveContext () {
        var error: NSError? = nil
        let managedObjectContext = self.managedObjectContext
        if managedObjectContext != nil {
            if managedObjectContext.hasChanges && !managedObjectContext.save(&error) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                //println("Unresolved error \(error), \(error.userInfo)")
                abort()
            }
        }
    }
    
    // Returns the managed object context for the application.
    // If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
    var managedObjectContext: NSManagedObjectContext {
    if !_managedObjectContext {
        let coordinator = self.persistentStoreCoordinator
        if coordinator != nil {
            _managedObjectContext = NSManagedObjectContext()
            _managedObjectContext!.persistentStoreCoordinator = coordinator
        }
        }
        return _managedObjectContext!
    }
    var _managedObjectContext: NSManagedObjectContext? = nil
    
    // Returns the managed object model for the application.
    // If the model doesn't already exist, it is created from the application's model.
    var managedObjectModel: NSManagedObjectModel {
    if !_managedObjectModel {
        let modelURL = NSBundle.mainBundle().URLForResource("DrinkX", withExtension: "momd")
        _managedObjectModel = NSManagedObjectModel(contentsOfURL: modelURL)
        }
        return _managedObjectModel!
    }
    var _managedObjectModel: NSManagedObjectModel? = nil
    
    // Returns the persistent store coordinator for the application.
    // If the coordinator doesn't already exist, it is created and the application's store added to it.
    var persistentStoreCoordinator: NSPersistentStoreCoordinator {
    if !_persistentStoreCoordinator {
        let storeURL = self.applicationDocumentsDirectory.URLByAppendingPathComponent("DrinkX.sqlite")
        var error: NSError? = nil
        _persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        if _persistentStoreCoordinator!.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: storeURL, options: nil, error: &error) == nil {
            /*
            Replace this implementation with code to handle the error appropriately.
            
            abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            
            Typical reasons for an error here include:
            * The persistent store is not accessible;
            * The schema for the persistent store is incompatible with current managed object model.
            Check the error message to determine what the actual problem was.
            
            
            If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
            
            If you encounter schema incompatibility errors during development, you can reduce their frequency by:
            * Simply deleting the existing store:
            NSFileManager.defaultManager().removeItemAtURL(storeURL, error: nil)
            
            * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
            [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true}
            
            Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
            
            */
            //println("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        }
        return _persistentStoreCoordinator!
    }
    var _persistentStoreCoordinator: NSPersistentStoreCoordinator? = nil
    
    // #pragma mark - Application's Documents directory
    
    // Returns the URL to the application's Documents directory.
    var applicationDocumentsDirectory: NSURL {
    let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls[urls.endIndex-1] as NSURL
    }

}

//
//  Drink.m
//  DrinkX
//
//  Created by William Kluss on 6/10/14.
//  Copyright (c) 2014 William Kluss. All rights reserved.
//

#import "Drink.h"
#import "Ingredient.h"


@implementation Drink

@dynamic name;
@dynamic summary;
@dynamic favorite;
@dynamic ingredients;

@end

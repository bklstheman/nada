//
//  DrinkXFavoritesTableViewController.swift
//  DrinkX
//
//  Created by William Kluss on 6/15/14.
//  Copyright (c) 2014 William Kluss. All rights reserved.
//

import UIKit

class DrinkXFavoritesTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {
    
    var favoriteDrinks : NSFetchedResultsController?
    
    override func viewDidLoad() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("refreshData:"), name: NSManagedObjectContextDidSaveNotification, object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let coreDataManage = CoreDataManager.sharedInstance()
        
        if(self.favoriteDrinks) {
            NSFetchedResultsController.deleteCacheWithName("")
        }
        self.favoriteDrinks = coreDataManage.createFavoriteDrinkFetchedResultController()
        self.favoriteDrinks!.delegate = self
        self.favoriteDrinks!.performFetch(nil)
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool)  {
        super.viewWillDisappear(animated)        
    }
    
    func refreshData(notification: NSNotification) {
        self.favoriteDrinks!.managedObjectContext.mergeChangesFromContextDidSaveNotification(notification)
    }

    // #pragma mark - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView?) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.favoriteDrinks!.fetchedObjects.count
    }
        
    override func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell? {
        let cell:UITableViewCell = self.tableView.dequeueReusableCellWithIdentifier("Cell") as UITableViewCell
                
        let drink = self.favoriteDrinks!.fetchedObjects[indexPath.row] as Drink
        cell.textLabel.text = drink.name
        
        return cell
    }
    
    override func tableView(tableView: UITableView!, didSelectRowAtIndexPath indexPath: NSIndexPath!) {
        
        let drinkDetailsVC = self.storyboard.instantiateViewControllerWithIdentifier("DrinkDetailsVC") as DrinkXDetailsViewController
        drinkDetailsVC.drink = self.favoriteDrinks!.fetchedObjects[indexPath.row] as? Drink
        self.navigationController.pushViewController(drinkDetailsVC, animated: true)
    }

    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return true;
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController!) {
        self.tableView.beginUpdates()
    }
    
    override func tableView(tableView: UITableView?, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath?) {
        if editingStyle == .Delete {
            let drinkName = self.favoriteDrinks!.fetchedObjects[indexPath!.row] as Drink
            drinkName.favorite = false
            self.favoriteDrinks!.managedObjectContext.save(nil);
        }
    }
    
    func controller(controller: NSFetchedResultsController!, didChangeObject anObject: AnyObject!, atIndexPath indexPath: NSIndexPath!, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath!) {
        switch(type) {
        case NSFetchedResultsChangeInsert:
            self.tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: UITableViewRowAnimation.None)
        case NSFetchedResultsChangeDelete:
            self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
        default:
            break;
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController!) {
        self.tableView.endUpdates()
    }
}

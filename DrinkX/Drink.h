//
//  Drink.h
//  DrinkX
//
//  Created by William Kluss on 6/10/14.
//  Copyright (c) 2014 William Kluss. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Ingredient;

@interface Drink : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * summary;
@property (nonatomic, retain) NSNumber * favorite;
@property (nonatomic, retain) NSSet *ingredients;
@end

@interface Drink (CoreDataGeneratedAccessors)

- (void)addIngredientsObject:(Ingredient *)value;
- (void)removeIngredientsObject:(Ingredient *)value;
- (void)addIngredients:(NSSet *)values;
- (void)removeIngredients:(NSSet *)values;

@end

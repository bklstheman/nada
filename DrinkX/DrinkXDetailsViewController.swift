//
//  DrinkXDetailsViewController.swift
//  DrinkX
//
//  Created by William Kluss on 6/15/14.
//  Copyright (c) 2014 William Kluss. All rights reserved.
//

import UIKit

class DrinkXDetailsViewController: UIViewController, UITableViewDelegate {

    @IBOutlet var drinkNameLabel : UILabel = nil
    @IBOutlet var drinkSegmentControl : UISegmentedControl = nil
    @IBOutlet var ingredientsTableView : UITableView = nil
    @IBOutlet var instructionsTextView : UITextView = nil
    var drink : Drink?
    var ingredients : Array<AnyObject>?
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.ingredients = drink!.ingredients.allObjects
        self.drinkNameLabel.text = drink!.name
        self.instructionsTextView.text = drink!.summary
                
        let favoriteButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: Selector("addToFavorite"))
        self.navigationItem.rightBarButtonItem = favoriteButton
        
    }
    
    //Table View Delegates
    func tableView(tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        return ingredients!.count
    }
    
    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell? {
        let cell:UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "Cell")
        var ingredient = self.ingredients?[indexPath.row] as Ingredient
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.text = ingredient.detail
        
        return cell
    }

    @IBAction func drinkSegmentControlPressed(sender : UISegmentedControl) {
        //We need to switch what we display. So whatever is hidden is no longer, and what is longer is hidden.
        //or some bullshit
        
        self.instructionsTextView.hidden = !self.instructionsTextView.hidden
        self.ingredientsTableView.hidden = !self.ingredientsTableView.hidden
    }
    
    func addToFavorite() {
        let coreDataManager = CoreDataManager.sharedInstance()
        if(!self.drink!.favorite.boolValue) {
            coreDataManager.addDrinkToFavorites(self.drink!.name)
        }
        
        let alertView = UIAlertController(title: "Success", message: "Drink added to Favorties", preferredStyle: UIAlertControllerStyle.Alert)
        alertView.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        presentViewController(alertView, animated: true, completion: nil)
    }
}
